﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConvertendoJasonParaClasse.Models
{
    class Computador
    {

        public string Marca { get; set; }
        public string Modelo { get; set;  }
        public int  Memoria { get; set; }
        public int SSD { get; set; }
        public string Processador { get; set; }
        public List<String> Softwares { get; set; }

        public Fornecedor Fornecedor { get;set; }

        
      

    }
    class Fornecedor
    {
        public string Rsocial { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
    }




}
