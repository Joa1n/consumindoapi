﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsumindoAPIUsuario.Models
{
    class Usuarios
    {
        public static IEnumerable<string> Opcionais { get; internal set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public int Telefone { get; set; }        

        public List<string> Conhecimentos { get; set; }

        public Endereco Endereco { get; set; }
        
        public List<object> Qualificacoes { get; set; }

    }

    class Endereco
    {
        public string Rua { get; set; }

        public int Numero { get; set; }

        public string Bairro { get; set; }

        public string Cidade { get; set; }

        public string Uf { get; set; }

    }
    class Qualificacoes
    {

    }
}
